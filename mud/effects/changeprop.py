# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect2
from .effect import Effect3
from mud.events import ChangePropEvent
from mud.events import ChangeNameEvent

class ChangePropEffect(Effect2):
    EVENT = ChangePropEvent

    def make_event(self):
        return self.EVENT(self.actor, self.object, self.yaml["modifs"])

class ChangeNameEffect(Effect3):
    EVENT = ChangeNameEvent

    def make_event(self):
        return self.EVENT(self.actor, self.object, self.yaml["modifs"])
