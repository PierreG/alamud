# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2
from .event import Event3

class ImprenierWithEvent(Event3):
	NAME="imprenier"
	
	def perform(self):
		if not self.object.has_prop("impreniable"):
			self.fail()
			return self.inform("impreniable.failed")
		self.inform("imprenier")
