# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2
from .event import Event3

class AssembleWithEvent(Event3):
	NAME = "assemblage"

	def perform(self):
		if not self.object.has_prop("assemblable") or not self.object2.has_prop("assemblable"):
			self.fail()
			return self.inform("assemblage.failed")
		self.inform("assemblage")
