from .action import Action2
from mud.events import ImprenierWithEvent
from .action import Action1
from .action import Action3

class ImprenierWithObject(Action3):
    EVENT = ImprenierWithEvent
    ACTION = "imprenier"
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_use"
